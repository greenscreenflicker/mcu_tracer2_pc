CC=gcc
CFLAGS=-I./ -O3 `pkg-config --cflags gtk+-3.0`
#CFLAGS=-I./ -O2 -pthread -I/usr/include/gtk-3.0 -I/usr/include/at-spi2-atk/2.0 -I/usr/include/at-spi-2.0 -I/usr/include/dbus-1.0 -I/usr/lib/x86_64-linux-gnu/dbus-1.0/include -I/usr/include/gtk-3.0 -I/usr/include/gio-unix-2.0 -I/usr/include/cairo -I/usr/include/pango-1.0 -I/usr/include/fribidi -I/usr/include/harfbuzz -I/usr/include/atk-1.0 -I/usr/include/cairo -I/usr/include/pixman-1 -I/usr/include/uuid -I/usr/include/freetype2 -I/usr/include/libpng16 -I/usr/include/gdk-pixbuf-2.0 -I/usr/include/libmount -I/usr/include/blkid -I/usr/include/glib-2.0 -I/usr/lib/x86_64-linux-gnu/glib-2.0/include

DEPS = hellomake.h
COMPILEFILES = main.o

%.o: %.c $(DEPS)
	$(CC) -c -o $@ $< $(CFLAGS)
	
all: $(COMPILEFILES)
	$(CC) -o mcu_tracer $(COMPILEFILES) `pkg-config --libs gtk+-3.0` -lpthread
	rm -f $(COMPILEFILES)

clean:
	rm -f mcu_tracer $(COMPILEFILES)


